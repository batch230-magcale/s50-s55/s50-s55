// import coursesData from '../data/coursesData';
import { Navigate } from 'react-router-dom';
import CourseCard from '../components/CourseCard';
import { Fragment, useEffect, useState, useContext } from 'react';

import UserContext from '../UserContext';

export default function Courses(){
	
	/*
		console.log("Contents of coursesData");
		console.log(coursesData);
		console.log(coursesData[0]);
	*/

	const {user} = useContext(UserContext);

	const [courses, setCourses] = useState([]); // in array yung course

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(result => result.json())
		.then(data => {
			console.log(data);
			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course} />
				)
			}))
		})
	}, []);

	// array method to display all courses
	/*const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} courseProp={course} />
		)
	})*/


	// return(
	// 	<Fragment>
	// 		<CourseCard courseProp={coursesData[0]}/>
	// 	</Fragment>
	// )

	return (
		(user.isAdmin)?
			<Navigate to="/admin" />
		:
		<>
			<h1> Courses </h1>
			{courses}
		</>
	)
}