import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';

import Swal from 'sweetalert2';

export default function Login(){

	// 3 - 
			// variable, setter function
	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);

	function authenticate(event){
		
		event.preventDefault();

		/*
			// Clear input fields after submission
			localStorage.setItem('email', email);

			setUser({
				email: localStorage.getItem('email')
			})

			setEmail('');
			setPassword('');
			
			console.log(`${email} has been verified. Welcome Back!`);
			alert('You are now logged in.');

			
			window.location.reload()
		*/

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			console.log('Check accessToken');
			console.log(data.accessToken);
			

			if(typeof data.accessToken !== 'undefined'){
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Zuitt!"
				})
			}
			else{
				Swal.fire({
					title: 'Authentication failed',
					icon: 'error',
					text: 'Check your login details and try again.'
				})
			}


		})

		setEmail('');
	}

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data.id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		if(email != '' && password != ''){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password])

	return(

		(user.id !== null)
		? // true - means email field is sucessfully set
		<Navigate to="/courses"/>
		: // false - means email field is not sucessfully set

		<Form onSubmit={event => authenticate(event)}>
			<h3>Login</h3>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                value = {email}
	                onChange = {event => setEmail(event.target.value)}
	                required
                />
            </Form.Group><br/>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password"
	                value = {password}
	                onChange= {event => setPassword(event.target.value)} 
	                required
                /><br/>
            </Form.Group>
            {isActive ? //true
	            <Button variant="success" type="submit" id="submitBtn" onClick={event => authenticate(event)}>
	            	Login
	            </Button>
            	: //false
            	<Button variant="success" type="submit" id="submitBtn" disabled>
            		Login
            	</Button>
        	}
        </Form>
	)




}