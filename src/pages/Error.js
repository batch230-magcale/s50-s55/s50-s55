/*import {Fragment} from 'react';
import {Link} from 'react-router-dom';

export default function Error(){

	return(
		<Fragment>
			<h1>Page Not Found</h1>
			<p>Go back to the <Link as={Link} to="/">homepage</Link>.</p>
		</Fragment>
	)
}*/

// s53 activity solution
import Banner from '../components/Banner';

export default function Error(){
	// We created an object variable named "data" to contain the properties we want to send in the
	const data = {
		title: "404 - Not Found",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back home"
	}

	return(
		<Banner data={data}/>
	)
}