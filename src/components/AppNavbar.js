// import Container from 'react-bootstrap/Container';
// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';
import {Container, Navbar, Nav} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import {useState, Fragment, useContext} from 'react';
import UserContext from '../UserContext';

export default function AppNavbar(){

	// const [user, setUser] = useState(localStorage.getItem("email"));
	const {user} = useContext(UserContext);
	console.log(user);


	/*function logout() {   
	 window.location.reload()
	}*/

	return(
		<Navbar bg="light" expand="lg">
		    <Container fluid>
		        <Navbar.Brand as={Link} to="/">Zuitt Booking (from AppNavbar.js)</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
		        <Navbar.Collapse id="basic-navbar-nav">
		            <Nav className="mr-auto">
			            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
			            <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
			            {(user.id !== null)?
			            	<Nav.Link as={NavLink} to="/logout" >Logout</Nav.Link>
			            	:
			            	<Fragment>
			            		<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
			            		<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
			            	</Fragment>
			        	}			            
		            </Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>
	)
}